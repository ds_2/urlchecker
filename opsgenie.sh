#!/usr/bin/env bash

FAILEDURL=$1
DURATION=${2:-0}
HTTPCODE=${3:-0}
CONNERR=${4:-false}
TAGS=${5:-urlcheck,test}
DRYRUN=${DRYRUN:-0}

echo "Failed url $FAILEDURL would be reported now!"

[[ -z "${OPSGEN_APIKEY}" ]] && exit 1
[[ -z "${OPSGEN_TEAM_NAME}" ]] && exit 2
[[ -z "${FAILEDURL}" ]] && exit 3

IFS=',' read -ra my_tag_array <<< "$TAGS"
printf -v joined '"%s",' "${my_tag_array[@]}"

# if [ -z "$OPSGEN_APIKEY" ]; then
#     echo "APIKey not defined!"
#     exit 1
# fi

shaValue=$(echo -n "$FAILEDURL" | sha256sum | awk '{print $1}')

OPSGEN_ALERT_URL=${OPSGEN_ALERT_URL:-https://api.eu.opsgenie.com/v2/alerts}
OPSGEN_PRIO=${OPSGEN_PRIO:-P3}

cat <<EOF >/tmp/alerts.json
{
    "message": "URL Check failed: $FAILEDURL",
    "alias": "${shaValue}",
    "description": "The following url failed to answer within time:\n\n${FAILEDURL}\n\nPlease check ;)",
    "responders": [
        {
            "type": "team",
            "name": "$OPSGEN_TEAM_NAME"
        }
    ],
    "tags": [${joined}"urlcheck"],
    "priority": "$OPSGEN_PRIO",
    "details": {
        "url": "$FAILEDURL",
        "reporter": "urlcheck",
        "user": "$(id)",
        "duration": ${DURATION},
        "httpCode": ${HTTPCODE},
        "connectionError": "${CONNERR}"
    }
}
EOF

if [ "$DRYRUN" -eq 0 ]; then
    curl -XPOST -H "Content-Type: application/json" -H "Authorization: GenieKey $OPSGEN_APIKEY" -d @/tmp/alerts.json $OPSGEN_ALERT_URL
else
    echo "Dryrun enabled, will not perform call to OpsGenie!"
    echo "File to send looks like:"
    cat /tmp/alerts.json
fi

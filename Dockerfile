FROM debian:buster-slim
ARG profile=release
RUN apt-get clean; apt-get update && apt-get install -y bash ca-certificates openssl curl && rm -rf /var/lib/apt/lists/*
COPY target/${profile}/urlcheck /usr/local/bin/urlcheck
COPY opsgenie.sh /opsgenie.sh
ENV ON_EACH_FAIL="/opsgenie.sh"
# ENV OPSGEN_APIKEY="12345"
# ENV OPSGEN_TEAM_NAME="My Team Name"
# ENV OPSGEN_PRIO="P1"
ENV URL_TIMEOUT=5
ENV RUST_LOG=info
ENV DRYRUN=0
ENV RETRY_COUNT=2
#ENV CSV_FILE=/data/urls.csv
USER 1000
CMD ["/usr/local/bin/urlcheck"]

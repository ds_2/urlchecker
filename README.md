# Dummy URL Checker

[![Docker Repository on Quay](https://quay.io/repository/ds2/url-checker/status "Docker Repository on Quay")](https://quay.io/repository/ds2/url-checker)

A dummy url checker written in Rust. The idea is to run this image in a kubernetes cluster as
a cronjob so that you get a urlcheck every minute etc. and you may report the errored urls
via a bash script to someone. As a sample I will provide here OpsGenie to test with.

## How to use

### Environment variables

* URL=https://www.pcwelt.de/ = instead of a CSV, try to test only this single url
* URL_TIMEOUT=5 = defines the read/connect timeout, in seconds
* ON_EACH_FAIL="/opsgenie.sh" = defines which command should be run for each failed url; the url is given as parameter
* OPSGEN_APIKEY="12345" = defines the opsgenie api key
* OPSGEN_TEAM_NAME="My Team Name" = defines the team name in OpsGenie to inform
* OPSGEN_PRIO="P1" = defines the priority of the alert, with P1 the most critical
* RUST_LOG=info = defines the log level for the logger; you may also want to use debug or trace here
* CSV_FILE=/data/urls.csv = defines where to look for the csv file with all the urls to test
* DRYRUN=1 = enables the DRYRUN

## How to build

Run:

    rustfmt src/*.rs
    cargo build
    cargo test
    docker build -t quay.io/ds2/url-checker:latest .

## How to Test

### Using the Gitlab Snapshot developer test image

    docker run -it --rm -e URL=https://www.pcwelt.de/ registry.gitlab.com/ds_2/urlchecker:develop

### Test a single url

    docker run -it --rm -e URL=https://www.pcwelt.de/ quay.io/ds2/url-checker:latest

### Test many urls

Create a CSV file and map it into the container.

    docker run -it --rm -e CSV_FILE=/test/test.csv -v $(pwd):/test quay.io/ds2/url-checker:latest

## Using your own error report script

You can use your own error script instead of the provided opsgenie.sh script. Your
script must be executable.

In your run config, set the script via:

    ON_EACH_FAIL=PATHTOYOURSCRIPT

The url check will call this script with these parameters:

1. URL
2. duration of the call
3. http error code
4. connection error boolean
5. the tags you provided in your config, comma separated string

Your script can now do its own thing.

## Perform a release

1. merge changes to the releases branch
2. update Cargo.toml and increase version
3. run the build: cargo build --release
4. commit changes
5. apply the tag based on the version from the TOML
6. push the tag
